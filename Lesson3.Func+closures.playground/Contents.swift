import Foundation

//Задание 1
func fibonacciNumber (count: Int) -> [Int] {
    var storyPointsNumbers = [Int]()
    if count < 1 {
        print("Ты, по ходу, перепутал. Число должно быть положительным")
    } else {
        var number1 = 1
        var number2 = 1
        storyPointsNumbers.append(number1)
        for _ in 0 ..< count - 1 {
            storyPointsNumbers.append(number2)
            let number = number1 + number2
            number1 = number2
            number2 = number
        }
        print("Отфибоначчено для \(count): \(storyPointsNumbers)")
    }
    return storyPointsNumbers
}

fibonacciNumber(count: 5)
fibonacciNumber(count: 7)
fibonacciNumber(count: -10)
print("_____________________")

//Задание 2
let printArrayFor = { (array:[Int]) in
    if array.count < 1 {
        print("Я не смогу вывести тебе массив по строкам")
    } else {
        for i in 0 ... array.count - 1 {
            print(array[i])
        }
    }
}

printArrayFor(fibonacciNumber(count: 4))
printArrayFor(fibonacciNumber(count: -5))

