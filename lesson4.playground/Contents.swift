import UIKit

class Field {
    let header: String
    let lenght : Int
    var placeholder: String?
    let code: Int?
    enum Priority: String {
        case high
        case medium
        case low
    }
    
    func lenghtBorder () -> Bool {
        return self.lenght <= 25 ? true : false
    }
    
    func changePlaceholder (newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
    
    init (header: String, lenght: Int, placeholder: String? = nil, code: Int? = nil, priority: Priority) {
        self.header = header
        self.lenght = lenght
        self.placeholder = placeholder
        self.code = code
        //self.priority = priority.rawValue - раньше тут шло назначение параметру priority rawValue из enum-а, но параметр пришлось удалить в 5 задании
    }
}

let nameField = Field(header: "Name", lenght: 25, placeholder: "Type your name", code: 1, priority: .high)
let surnameField = Field(header: "Surname", lenght: 25, placeholder: "Type your surname", code: 2, priority: .high)
let ageField = Field (header: "Age", lenght: 3, placeholder: "Type your age", code: 3, priority: .medium)
let cityField = Field (header: "City", lenght: 15, placeholder: "City", code: 4, priority: .low)

//Задание 2
let passportIssuedField = Field (header: "Passport Issued", lenght: 45, placeholder: "You'll never remember this", code: 5, priority: .medium)
passportIssuedField.lenghtBorder()

let genderField = Field(header: "Gender", lenght: 5, placeholder: "Only two genders for now", code: 6, priority: .medium)
genderField.lenghtBorder()

genderField.placeholder
genderField.changePlaceholder(newPlaceholder: "You've have 100 more genders to choose!")
genderField.placeholder

//Задание 3
//Делаем опциональными placeholder и code
let passportNumberField = Field(header: "Passport Number", lenght: 6, priority: .low)

//Задание 4

//Бахнул enum в инициализатор, свичом проверяем выбранное значение и определяем priority в зависимости от выбранного значения. Намучался с этим больше всего, если есть варианты лучше, буду рад знать

//Задание 5
//Переписал структуру отдельно с удалением всего ненужного

struct StructField {
    let header: String
    let lenght : Int
    var placeholder: String?
    let code: Int?
    
    mutating func lenghtBorder () -> Bool {
        return self.lenght <= 25 ? true : false
    }
    
    mutating func changePlaceholder (newPlaceholder: String) {
        self.placeholder = newPlaceholder
    }
}

var structNameField = StructField(header: "Name", lenght: 15, placeholder: "Another name", code: 1)
var structSurnameField = StructField(header: "Surname", lenght: 15, placeholder: "Another surname", code: 2)
structNameField.lenghtBorder()
structSurnameField.changePlaceholder(newPlaceholder: "I am zaebalsya")
