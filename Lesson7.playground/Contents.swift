import UIKit

var evenArray = [Int]()
for i in 0...50 {
    if i % 2 == 0 {
        print(i)
        evenArray.append(i)
    }
}
print(evenArray)

var index = 0
while index < 10 {
    print(evenArray[index])
    index += 1
}

// Задание 2
typealias sportsmanGrades = [String: [judgesNames.RawValue: Double]]

enum judgesNames: String {
    case firstJudge = "Олег"
    case secondJudge = "Анна"
    case thirdJudge = "Михаил"
}

var listOfGrades : sportsmanGrades = [:]
listOfGrades.updateValue([judgesNames.firstJudge.rawValue: 6, judgesNames.secondJudge.rawValue: 6, judgesNames.thirdJudge.rawValue: 8], forKey: "Иван")
listOfGrades.updateValue([judgesNames.firstJudge.rawValue: 10, judgesNames.secondJudge.rawValue: 10, judgesNames.thirdJudge.rawValue: 10], forKey: "Великий нефритовый стержень Xi")
print(listOfGrades)

for (name, grades) in listOfGrades {
    var sumOfGrades = Double()
    for grade in grades.values {
        sumOfGrades += grade
    }
    let averagePoint = sumOfGrades / Double(grades.count)
    print("Средний балл участника \(name): \(averagePoint)")
}
