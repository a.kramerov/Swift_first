import UIKit

extension Int {
    func sumOfNumerics () -> Int {
        let arrayOfNumerics: [Int] = String(self).compactMap { Int(String($0)) }
        return Int(arrayOfNumerics.reduce (0, +))
    }
}

123.sumOfNumerics()
731.sumOfNumerics()

//Задание 2

extension String {
    var toInt: Int { return Int(self) ?? 0 }
}

"53".toInt
"920".toInt
"Твои мечты".toInt


//Задание 3

protocol CountOperations {
    func countWithOperation () -> Double
}

class Multiplication: CountOperations {
    var firstNumber: Double
    var secondNumber: Double
    init (firstNumber: Double, secondNumber: Double) {
        self.firstNumber = firstNumber
        self.secondNumber = secondNumber
    }
    func countWithOperation() -> Double {
        return firstNumber * secondNumber
    }
}

class Division: CountOperations {
    var firstNumber: Double
    var secondNumber: Double
    init (firstNumber: Double, secondNumber: Double) {
        self.firstNumber = firstNumber
        self.secondNumber = secondNumber
    }
    func countWithOperation() -> Double {
        return firstNumber / secondNumber
    }
}

class Addition: CountOperations {
    var firstNumber: Double
    var secondNumber: Double
    init (firstNumber: Double, secondNumber: Double) {
        self.firstNumber = firstNumber
        self.secondNumber = secondNumber
    }
    func countWithOperation() -> Double {
        return firstNumber + secondNumber
    }
}

class Subtraction: CountOperations {
    var firstNumber: Double
    var secondNumber: Double
    init (firstNumber: Double, secondNumber: Double) {
        self.firstNumber = firstNumber
        self.secondNumber = secondNumber
    }
    func countWithOperation() -> Double {
        return firstNumber - secondNumber
    }
}

class Calculator {
    func calculate (operation: CountOperations) {
        print(operation.countWithOperation())
    }
}

var calculator = Calculator()
calculator.calculate(operation: Division(firstNumber: 10, secondNumber: 3))

calculator.calculate(operation: Multiplication(firstNumber: 5, secondNumber: 6))
calculator.calculate(operation: Subtraction(firstNumber: 5, secondNumber: 6))
calculator.calculate(operation: Addition(firstNumber: 5, secondNumber: 6))
calculator.calculate(operation: Division(firstNumber: 5, secondNumber: 6))
