import UIKit

// Задача 1

let milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// Задача 2.

var milkPrice: Double = 3

//Задача 3

milkPrice = 4.20

//Задача 4

var milkBottleCount: Int? = 20
var profit: Double = 0
if let anyBottles = milkBottleCount {
    profit = Double(anyBottles) * milkPrice
    print(profit)
} else {
    print("Кажется сегодня кто-то останется без обеда")
}

/*Принудительное развёртывание не рекомендуется использовать потому что:
1. Если программа в случае принудительного развёртывания наткнётся на nil, то она упадёт с ошибкой
2. Мы перекладываем ответственность за безопасность программы с самой программы на разработчика, что увеличивает риск "человеческого фактора", потери контекста, появлению непредвиденных ошибок
3. Применение принудительного развёртывания может сигнализировать об использовании опционалов там, где они не нужны */

//Задача 5

var employeesList: [String] = []
employeesList.append("Иван")
employeesList.append("Марфа")
employeesList.append("Андрей")
employeesList.append("Пётр")
employeesList.append("Геннадий")

//Задача 6

var isEveryoneWorkHard = false
let workingHours: Float = 39.5
if workingHours >= 40 {
    isEveryoneWorkHard = true
    print("\(isEveryoneWorkHard), получи тарелка рис")
} else {
    print("\(isEveryoneWorkHard), месяц без тарелка рис")
}
//объявил workingHours как Float, чтобы система адекватно учитывала дробное количество отработанных часов. Если в этом нет необходимости, то workingHours - Int
