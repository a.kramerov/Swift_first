import UIKit

open class Shape {
    open func calculateArea () -> Double {
        fatalError("not implemented")
    }
    open func calculatePerimeter () -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    override func calculatePerimeter() -> Double {
        return 2 * (height + width)
    }
    override func calculateArea() -> Double {
        return height * width
    }
    
    init(height: Double, wigth: Double) {
        self.height = height
        self.width = wigth
    }
}

class Circle: Shape {
    private let radius: Double
    
    override func calculateArea() -> Double {
        return Double.pi * pow(radius, 2)
    }
    override func calculatePerimeter() -> Double {
        return 2 * Double.pi * radius
    }
    
    init(radius: Double) {
        self.radius = radius
    }
}

class Square: Shape {
    private let side: Double
    
    override func calculateArea() -> Double {
        return pow(side, 2)
    }
    override func calculatePerimeter() -> Double {
        return 4 * side
    }
    
    init(side: Double) {
        self.side = side
    }
}

var area: Double
var perimeter: Double
var sumOfAreas: Double = 0
var sumOfPerimeters: Double = 0
//увидел в тредах, что в результате задания требуется сумма площадей и периметров фигур в массиве. Я этого из задания не понял, но тоже можно бахнуть
var shapes = [Shape]()
shapes.append(Rectangle(height: 5, wigth: 3))
shapes.append(Circle(radius: 4))
shapes.append(Square(side: 3))

for shape in shapes {
    area = shape.calculatePerimeter()
    perimeter = shape.calculateArea()
    sumOfAreas += area
    sumOfPerimeters += perimeter
    print("Площадь фигуры: \(area)")
    print("Периметр фигуры: \(perimeter)")
}

print("Сумма всех площадей равна: \(sumOfAreas)")
print("Сумма всех периметров равна: \(sumOfPerimeters)")

//Задание 2

func findIndexWithGenerics<T: Equatable>(ofElement valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndex = findIndexWithGenerics(ofElement: "лама", in: arrayOfString) {
    print("Индекс ламы: \(foundIndex)")
} else {
    print("Без обид, твоего элемента нет")
}

let arrayOfDouble: [Double] = [3.2, 6.5, 1.0, 7.3, 2.8]
if let foundIndex = findIndexWithGenerics(ofElement: 6.5, in: arrayOfDouble) {
    print("Индекс числа 6.5: \(foundIndex)")
} else {
    print("Без обид, твоего элемента нет")
}

let arrayOfInt: [Int] = [7, 0, 83, 24, 49, 3, 5, 4]
if let foundIndex = findIndexWithGenerics(ofElement: 3, in: arrayOfInt) {
    print("Индекс числа 3: \(foundIndex)")
} else {
    print("Без обид, твоего элемента нет")
}


//И, если не сложно, вопрос чисто для себя - такое решение валидно, или чем-то хуже предлагаемой выше функции?
func myFindIndexWithGenerics<T: Equatable>(ofElement valueToFind: T, in array: [T]) -> String {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return "Индекс элемента \(valueToFind): \(index)"
        }
    }
    return "Без обид, твоего элемента нет"
}

print(myFindIndexWithGenerics(ofElement: "лама", in: arrayOfString))
